# Nookibodo
Turn your voice into chat messages for Animal Crossing: New Horizons

[![Donate](https://img.shields.io/badge/Donate-PayPal-green.svg)](https://www.paypal.me/Stjinchan)

<img src="images/banner.png"
      alt="banner"/>

## Screenshots

<img src="images/screenshot-0.gif"
      alt="nintendo_switch"
      height="500"/>

<img src="images/screenshot-1.gif"
      alt="android_phone"
      height="500"/>

## About

Features:
- Send messages automatically after speaking.
- Messages that are too long are automatically split and you can easily send the other parts of the message via the UI.
- Instead of 1 history message that nintendo offers, Nookibodo offers up to 7 recent messages.
- Supported by all languages ​​that the Google Voice API supports.

Features planned:
- Preset messages
- Countdown before sending messages
- Bypass the .blur() of the webpage to automatically send other parts of the message
- Play store release(?)

## Downloads

[Download on Gitlab](https://gitlab.com/Stjin/nookibodo/-/releases)
## Credits

Licensed under Apache License Version 2

This app uses the following libraries:
[Android Speech](https://github.com/gotev/android-speech)
[WaveView](https://github.com/narayanacharya6/WaveView)
