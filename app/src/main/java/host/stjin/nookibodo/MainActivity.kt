package host.stjin.nookibodo

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
    }


    private fun howToUseDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setMessage(R.string.how_to_use_nookibodo)
        builder.setPositiveButton(
            "OK"
        ) { dialog, _ ->
            dialog.dismiss()
        }

        builder.setCancelable(false)
        builder.create().show()
    }

    private fun grantMicrophonePermissions() {
        if (checkSelfPermission(Manifest.permission.RECORD_AUDIO)
            != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.RECORD_AUDIO),
                2003
            )
        }
    }

    override fun onStart() {
        super.onStart()
        checkForInputSettings()
        checkForMicrophonePermissions()
    }

    private fun checkForMicrophonePermissions() {
        val permission = ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.RECORD_AUDIO
        )

        if (permission != PackageManager.PERMISSION_GRANTED) {
            Log.i("MAINACTIVITY", "Permission to record denied")
            makeRequest()
        }
    }

    private fun makeRequest() {
        val builder = AlertDialog.Builder(this)
        builder.setMessage(R.string.microphone_permissions_required)
        builder.setPositiveButton(
            "OK"
        ) { dialog, _ ->
            grantMicrophonePermissions()
            dialog.dismiss()
        }

        builder.setCancelable(false)
        builder.create().show()
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 2003) {
            if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                makeRequest()
            } else {
                howToUseDialog()
            }
        }
    }


    private fun checkForInputSettings() {
        var enabled = false
        try {
            val immService =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            val inputMethodInfoCache =
                InputMethodInfoCache(immService, packageName)
            enabled = inputMethodInfoCache.isInputMethodOfThisImeEnabled
        } catch (e: Exception) {
            Log.e(
                "MAINACTIVITY",
                "Exception in check if input method is enabled",
                e
            )
        }
        if (!enabled) {
            val context: Context = this
            val builder = AlertDialog.Builder(this)
            builder.setMessage(R.string.setup_message)
            builder.setPositiveButton(
                "OK"
            ) { dialog, _ ->
                val intent =
                    Intent(Settings.ACTION_INPUT_METHOD_SETTINGS)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                context.startActivity(intent)
                dialog.dismiss()
            }

            builder.setCancelable(false)
            builder.create().show()
        }
    }

}
