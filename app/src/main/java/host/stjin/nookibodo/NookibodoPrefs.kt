package host.stjin.nookibodo

import android.content.Context
import android.content.SharedPreferences
import android.text.TextUtils


class NookibodoPrefs(context: Context) {


    private val nookibodoPrefs: SharedPreferences = context.getSharedPreferences("nookibodo_keyboard", 0)


    fun getLastSentenceValues(): java.util.ArrayList<String>? {
        return getListString("last_sentence")
    }

    fun setLastSentenceValues(last_sentence: ArrayList<String>) {
        putListString("last_sentence", last_sentence)
    }

    fun clearFinishLastSentenceValues() {
        nookibodoPrefs.edit().remove("last_sentence").apply()
    }

    @ExperimentalStdlibApi
    fun addSentenceToHistory(text: String) {
        val history = getSentenceHistory()

        // If the history has over 7 items. Remove the last item
        if (history?.size ?: 0 > getHistorySaveAmount()) {
            history?.removeLast()
        }

        if (text != "") {
            // Add the last sentence to the beginning of the list
            history?.add(0, text)
        }

        // Push the list.
        setSentenceHistory(history)
    }

    private fun putListString(
        key: String?,
        stringList: java.util.ArrayList<String>
    ) {
        checkForNullKey(key)
        val myStringList = stringList.toTypedArray()
        nookibodoPrefs.edit().putString(key, TextUtils.join("‚‗‚", myStringList)).apply()
    }

    private fun checkForNullKey(key: String?) {
        if (key == null) {
            throw NullPointerException()
        }
    }


    private fun getListString(key: String?): java.util.ArrayList<String>? {
        return java.util.ArrayList(
            listOf(
                *TextUtils.split(
                    nookibodoPrefs.getString(key, ""),
                    "‚‗‚"
                )
            )
        )
    }

    fun getSentenceHistory(): java.util.ArrayList<String>? {
        return getListString("sentence_history")
    }

    private fun setSentenceHistory(sentence_history: java.util.ArrayList<String>?) {
        if (sentence_history != null) {
            putListString("sentence_history", sentence_history)
        }
    }


    private fun getHistorySaveAmount(): Int {
        return nookibodoPrefs.getInt("save_history_amount", 5)
    }

    fun setHistorySaveAmount(save_history_amount: Int) {
        nookibodoPrefs.edit().putInt("save_history_amount", save_history_amount).apply()
        clearHistory()
    }

    private fun clearHistory() {
        nookibodoPrefs.edit().remove("sentence_history").apply()
    }


}