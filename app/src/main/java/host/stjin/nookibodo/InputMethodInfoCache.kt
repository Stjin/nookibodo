/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package host.stjin.nookibodo

import android.view.inputmethod.InputMethodManager

/**
 * Enrichment class for InputMethodManager to simplify interaction and add functionality.
 */
class InputMethodInfoCache(
    private val mImm: InputMethodManager,
    private val mImePackageName: String
) {

    @get:Synchronized
    val isInputMethodOfThisImeEnabled: Boolean
        get() {
            for (imi in mImm.enabledInputMethodList) {
                if (imi.packageName == mImePackageName) {
                    return true
                }
            }
            return false
        }

}